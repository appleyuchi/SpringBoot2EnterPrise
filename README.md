这个repository对应的是书籍《Spring Boot应用实战》


|文件夹内容|备注|
|---|---|
|[第1章-SpringBoot入门－完成](第1章-SpringBoot入门－完成)||
|[第2章-SpringBoot核心－完成](第2章-SpringBoot核心－完成)||
|[第3章-SpringBoot的Web开发-完成](第3章-SpringBoot的Web开发-完成)||
|[第4章-SpringBoot的数据访问-完成](第4章-SpringBoot的数据访问-完成)||
|[第5章-SpringBoot的热部署与单元测试-完成](第5章-SpringBoot的热部署与单元测试-完成)||
|[第6章-SpringBoot的Security安全控制-完成](第6章-SpringBoot的Security安全控制-完成)||
|[第7章-信息管理系统-完成](第7章-信息管理系统-完成)||
|[依赖注入与IOC控制反转](依赖注入与控制反转)|[解析链接](https://blog.csdn.net/appleyuchi/article/details/109012777)|
|[learn_component-完成](learn_component-完成)|比较Component和Configuration区别的实验


因为这本书最终是描述一个半成品的项目,所以第6章和第7章的代码只是跑通,没有进行自己的笔记记录