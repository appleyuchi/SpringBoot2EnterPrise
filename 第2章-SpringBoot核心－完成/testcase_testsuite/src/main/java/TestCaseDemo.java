import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCaseDemo extends TestCase{

    @Test
    public void testAdd(){
        Calculator ca = new Calculator();
        assertEquals(14, ca.add(5, 9));

    }

    @Test
    public void testPlus(){
        Calculator ca=new Calculator();
        assertEquals(-4,ca.plus(5, 9));
    }

}