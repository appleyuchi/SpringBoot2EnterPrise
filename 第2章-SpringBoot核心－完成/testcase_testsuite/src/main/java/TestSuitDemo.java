import junit.framework.Test;
import junit.framework.TestSuite;

public class TestSuitDemo{

    public static Test suite(){
        //创建TestSuite对象
        TestSuite testSuite=new TestSuite("All Test From TestCaseExample");
        //为TestSuite添加一个测试用例集合，参数为：ClasstestClass
        //通过参数可以知道，其实该参数就是TestCase的子类
        testSuite.addTestSuite(TestCaseDemo.class);
        //创建具体的测试用例
        Test test = TestSuite.createTest(TestCaseDemo.class, "testAdd");
        Test test1 = TestSuite.createTest(TestCaseDemo.class, "testPlus");
        //添加一个具体的测试用例
        testSuite.addTest(test);
        testSuite.addTest(test1);
        return testSuite;
    }
}