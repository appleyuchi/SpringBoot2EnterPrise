package org.fkit.thymeleaftest.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.fkit.thymeleaftest.domain.Book;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

@Controller
public class ThymeleafController {
	
	/*
	 * 保存数据到作用范围域，用于测试Thymeleaf表达式访问数据
	 * */
	@RequestMapping("/regexptest")
	public String regexptest(HttpServletRequest request,HttpSession session)
	{
		System.out.println(request.getParameter("loginName"));
		System.out.println(request.getParameter("password"));

//接受页面请求中的参数（这里其实是视图函数）
//接收参数后直接打印，其实这里应该扩展为去数据库进行校验


		request.setAttribute("book", "疯狂Spring Boot讲义");
		// 保存数据到session作用范围域当中

		session.setAttribute("school", "疯狂软件");
		// 保存数据到ServletContext（application）作用范围域当中
		request.getServletContext().setAttribute("name", "Thymeleaf模板引擎");
		return "success1";
	}

	/*
	 * 保存数据到作用范围域，用于测试Thymeleaf的条件判断
	 * */
	@RequestMapping("/iftest")
	public String iftest(WebRequest webRequest)
	{
		// 保存数据到request作用范围域，Spring MVC更推荐使用WebRequest
		webRequest.setAttribute("username", "fkit", RequestAttributes.SCOPE_REQUEST);
		webRequest.setAttribute("age", 21, RequestAttributes.SCOPE_REQUEST);
		webRequest.setAttribute("role", "admin", RequestAttributes.SCOPE_REQUEST);
//s:变量名称
//o:变量取值
//role:变量
		return "success2";
	}
	
	/*
	 * 保存数据到作用范围域，用于测试Thymeleaf的循环获取数据
	 * */
	@RequestMapping("/eachtest")
	public String eachtest(WebRequest webRequest)
	{
		// 模拟数据库数据保存到List集合
		List<Book> books = new ArrayList<>();

//		下面将对象初始化以后加入列表
		books.add(new Book(1, "疯狂Java讲义-补充", "java.jpg", "李刚 编著", 109.00));
		books.add(new Book(2, "轻量级Java EE企业应用实战", "ee.jpg", "李刚 编著", 108.00));
		books.add(new Book(3, "Spring+MyBatis应用实战", "SpringMyBatis.jpg", "疯狂软件 编著", 58.00));
		books.add(new Book(4, "疯狂Android讲义", "android.jpg", "李刚 编著", 108.00));
		books.add(new Book(5, "疯狂Ajax开发", "ajax.jpg", "李刚 编著", 79.00));
		// 保存数据到request作用范围域
		webRequest.setAttribute("books", books, RequestAttributes.SCOPE_REQUEST);
		/*这里的数据是通过thymeleaf传递到网页上*/
//		 这里“books”是与html页面对应的变量名
//		上面的webRequest是用来设置传递给html的变量的

		return "success3";
	}
	
}




