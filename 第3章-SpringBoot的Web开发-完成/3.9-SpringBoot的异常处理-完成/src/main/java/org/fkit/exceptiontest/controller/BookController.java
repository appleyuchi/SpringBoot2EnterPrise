package org.fkit.exceptiontest.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BookController extends BaseController
{
	
	@RequestMapping("/find")
	public String find() throws Exception
	{
		System.out.println("find()......");
		int i = 5/0;
		return "success";
//		因为抛出异常，所以不可能返回success.html,所以resources文件夹中没有这个文件
	}

}
