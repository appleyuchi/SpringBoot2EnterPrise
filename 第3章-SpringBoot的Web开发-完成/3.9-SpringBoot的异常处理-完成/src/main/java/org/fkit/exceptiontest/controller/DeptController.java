package org.fkit.exceptiontest.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DeptController
{

	@RequestMapping("/add")
	public String add(String deptname) throws Exception
	{
		System.out.println("add()......");
		if(deptname == null )
		{
			throw new NullPointerException("部门名称不能为空!");
		}
		return "success";
		//		因为抛出异常，所以不可能返回success.html,所以resources文件夹中没有这个文件
	}
	
}
