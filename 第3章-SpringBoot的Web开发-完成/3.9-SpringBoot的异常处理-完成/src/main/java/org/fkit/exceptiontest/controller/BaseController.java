package org.fkit.exceptiontest.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

public class BaseController
{

	@ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		System.out.println("BaseController defaultErrorHandler()......");

		//下面这些代码的作用是把异常的抛出信息渲染到html上去。
		ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);//获取路由文件中的异常设定
        mav.addObject("url", req.getRequestURL());//获取路由文件中的具体路由
        mav.setViewName("error");//这个设定的是抛出异常的时候加载的html页面文件是error.html
        return mav;
    }
	
}
