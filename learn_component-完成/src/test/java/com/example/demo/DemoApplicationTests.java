package com.example.demo;
import com.example.demo.bean.Driver;
import com.example.demo.bean.Car;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


//@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests
{

    @Autowired
    private Car car;

    @Autowired
    private Driver driver;

    @Test
    public void contextLoads()
    {
        boolean result = driver.getCar() == car;
        System.out.println(result ? "同一个car" : "不同的car");
    }

}

