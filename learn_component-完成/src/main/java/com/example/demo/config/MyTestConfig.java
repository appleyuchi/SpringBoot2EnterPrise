package com.example.demo.config;
import com.example.demo.bean.Car;
import com.example.demo.bean.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


//这个文件是为DemoApplicationTests服务的，不可删除。



//Configuration会授予对象一些成员函数。

//@Component
@Configuration
public class MyTestConfig
{

    @Bean
    public Driver driver()
    {
        Driver driver = new Driver();
        driver.setId(1);
        driver.setName("driver");
        driver.setCar(car());
        return driver;
    }

    @Bean
    public Car car()
    {
        Car car = new Car();
        car.setId(1);
        car.setName("car");
        return car;
    }
}