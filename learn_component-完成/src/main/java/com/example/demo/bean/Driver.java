package com.example.demo.bean;

import lombok.Data;

//lombok的作用可以参考:
//https://www.zhihu.com/question/42348457


@Data
public class Driver
{

    private long id;

    private String name;

    private Car car;

}