package com.example.demo.bean;


import lombok.Data;

@Data
public class Car {

    private long id;

    private String name;

}