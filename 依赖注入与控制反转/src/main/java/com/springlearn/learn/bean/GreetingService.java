// 此类就是ioc容器中的一个bean，内部属性通过外部注入
// @Service的作用就是声明他是一个bean
// @Autowired的作用就是依赖注入

package com.springlearn.learn.bean;
import com.springlearn.learn.lang.Language;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GreetingService{

    @Autowired
    private Language language;

    public GreetingService() {

    }

    public void sayGreeting() {
        String greeting = language.getGreeting();
        System.out.println("Greeting:" + greeting);
    }
}