package com.springlearn.learn.bean;

import java.util.Date;

import org.springframework.stereotype.Repository;



//生成一个动态的bean
@Repository
public class MyRepository
{
    public String getAppName()
    {
        return "Hello my first Spring App";
    }

    public Date getSystemDateTime()
    {
        return new Date();
    }
}