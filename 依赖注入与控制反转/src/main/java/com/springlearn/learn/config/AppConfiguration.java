// 此类是一个定义bean和集中bean的文件
// @Configuration声明这个类是定义bean的
// @ComponentScan扫描bean目录
// @Bean(name="language") 定义了一个名为language的bean，只要访问此bean就会自动调用getLanguage方法

package com.springlearn.learn.config;

import com.springlearn.learn.lang.Language;
import com.springlearn.learn.langimpl.Chinese;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.springlearn.learn.bean"})
public class AppConfiguration
{

    @Bean(name="language")
    public Language getLanguage()
    {
        return new Chinese();
    }
}