package com.springlearn.learn.lang;

public interface Language{
    public String getGreeting();
    public String getBye();
}