// 此类是程序的启动类
// AnnotationConfigApplicationContext会调用配置类
// 通过调用context.getBean("language");会创建Chinese类实例
// context.getBean("greetingService");greetingService其中的@Autowired会将language实例注入

package com.springlearn.learn;

import com.springlearn.learn.bean.GreetingService;
import com.springlearn.learn.bean.MyComponent;
import com.springlearn.learn.config.AppConfiguration;
import com.springlearn.learn.lang.Language;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class DemoApplication {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);

        System.out.println("-------------");

        Language language = (Language)context.getBean("language");
        System.out.println("Bean Language: "+ language);
        System.out.println("Call language.sayBye(): "+ language.getBye());

        GreetingService service = (GreetingService) context.getBean("greetingService");
        service.sayGreeting();
        System.out.println("----------");

        MyComponent myComponent = (MyComponent) context.getBean("myComponent");
        myComponent.showAppInfo();
    }
}