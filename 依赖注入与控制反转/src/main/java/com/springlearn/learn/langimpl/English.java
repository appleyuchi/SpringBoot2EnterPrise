package com.springlearn.learn.langimpl;
import com.springlearn.learn.lang.Language;

public class English implements Language{
    @Override
    public String getGreeting(){
        return "Hello";
    }

    @Override
    public String getBye() {
        return "Bye bye";
    }
}