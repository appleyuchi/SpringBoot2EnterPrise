
这一章，略过很多实验，是因为连接数据库的工具不需要掌握那么多种，没有意义．

create database springboot<br>
需要在数据库springboot中执行的建表语句:<br>create table tb_role
(
    id        bigint auto_increment
        primary key,
    authority varchar(255) null
)
    engine = MyISAM;
<br>
\------------------------------------------------------
create table tb_user
(
    id         bigint auto_increment
        primary key,
    login_name varchar(255) null,
    password   varchar(255) null,
    username   varchar(255) null
)
    engine = MyISAM;
<br>
\------------------------------------------------------
create table tb_user_role
(
    user_id bigint not null,
    role_id bigint not null
)
    engine = MyISAM;

create index FK7vn3h53d0tqdimm8cp45gc0kl
    on tb_user_role (user_id);

create index FKea2ootw6b6bb0xt3ptl28bymv
    on tb_user_role (role_id);
\------------------------------------------------------
<br>需要执行的语句:<br>INSERT into `tb_role`(`id`,`authority`) values (1,'ROLE_ADMIN'),(2,'ROLE_DBA'),(3,'ROLE_USER');
\------------------------------------------------------
INSERT INTO `tb_user`(`id`,login_name,`password`,`username`)VALUES
(1,'admin','$2a$10$KRVhKjIvEQQ1ENWM0k.Iwe8Dr6HPdu56Rfn9qJ6FFrpPFJf6IaiN.','管理员'),
(2,'fkit','$2a$10$KRVhKjIvEQQ1ENWM0k.Iwe8Dr6HPdu56Rfn9qJ6FFrpPFJf6IaiN.','疯狂软件');
\------------------------------------------------------
INSERT INTO `tb_user_role`(`user_id`,`role_id`)values(1,1),(1,2),(2,3);</pre>

\------------------------------------------------------
<br><br>普通账户<br>用户名:fkit<br>密码:123456<br><br>管理员账户:<br>用户名:admin<br>密码:123456

由于书上给的admin的密码是错误的，所以这里改成和fkit一样的密码



