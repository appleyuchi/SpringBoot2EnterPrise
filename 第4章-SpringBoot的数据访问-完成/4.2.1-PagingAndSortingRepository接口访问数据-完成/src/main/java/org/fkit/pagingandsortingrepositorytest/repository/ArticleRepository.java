package org.fkit.pagingandsortingrepositorytest.repository;

import org.fkit.pagingandsortingrepositorytest.bean.Article;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ArticleRepository extends PagingAndSortingRepository<Article, Integer> {

}//表格对应的bean对象，Integer是主键类型
