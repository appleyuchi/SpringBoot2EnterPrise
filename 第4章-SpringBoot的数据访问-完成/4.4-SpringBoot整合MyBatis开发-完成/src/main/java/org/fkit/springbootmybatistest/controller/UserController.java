package org.fkit.springbootmybatistest.controller;
import java.util.List;

import javax.annotation.Resource;

import org.fkit.springbootmybatistest.bean.User;
import org.fkit.springbootmybatistest.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/user")
public class UserController
{
	
	// 注入UserService
	@Resource
	private UserService userService;
	
	@RequestMapping("/insertUser")
	public String insertUser(User user)
	{
		return "插入数据["+userService.insertUser(user)+"]条";
	}


	@RequestMapping("/insertGetKey")
	public User insertGetKey(User user)
	{
		userService.insertGetKey(user);
		return user ;
	}

//	查
	@RequestMapping("/selectByUsername")
	public User selectByUsername(String username)//接受参数
	{
		return userService.selectByUsername(username);
	}



//	查
	@RequestMapping("/findAll")
	public List<User> findAll()
	{
		return userService.findAll();
	}


//	增
	@RequestMapping("/update")
	public void update(User user)
	{
		userService.update(user);
	}


//	删
	@RequestMapping("/delete")
	public void delete(Integer id)
	{
		userService.delete(id);
	}
}
