package org.fkit.crudrepositorytest.service;

import java.util.Optional;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.fkit.crudrepositorytest.bean.User;
import org.fkit.crudrepositorytest.repository.UserRepository;
import org.springframework.stereotype.Service;




//這個文件下面就是一些增刪改查操作
@Service
public class UserService {

	// 注入UserRepository
	@Resource
	private UserRepository userRepository;




	//－－－－－增－－－－－－－－－－

	@Transactional
	public User save(User User) {
		return userRepository.save(User);
	}




	//－－－－－刪－－－－－－－－－－
	@Transactional
	public void delete(int id) {
		userRepository.deleteById(id);
		
	}



//－－－－－查－－－－－－－－－－
//返回所有User对象

	public Iterable<User> getAll() {
		return userRepository.findAll();
	}
	
//返回id对应的User对象

	public User getById(Integer id) {
		// 根据id查询出对应的持久化对象
		Optional<User> op = userRepository.findById(id);
		 return op.get();
	}






//－－－－－改－－－－－－－－－－
	@Transactional
	public void update(User user) {
		// 直接调用持久化对象的set方法修改对象的数据
		user.setUsername("孙悟空");
		user.setLoginName("swk");
	}

}
